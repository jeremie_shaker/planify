(function(){
'use strict';

var db = function($http){
	
	var hasRecord = function (record){
		return $http.post('queries/has_record.php', record)
					.then(function(response){
							return response.data;
					});
	};
	
	var getRecord = function (record){
		return $http.post('queries/get_record.php', record)
					.then(function(response){
							return response.data;
					});
	};
	
	var selectRecord = function (record){
		return $http.post('queries/select_record.php', record)
					.then(function(response){
							return response.data;
					});
	};
	
	var insertRecord = function (record){
		return $http.post('queries/insert_record.php', record)
					.then(function(response){
						return response.data;
					});
	};	
	
	var deleteRecord = function (record){
		return $http.post('queries/delete_record.php', record)
					.then(function(response){
						return response.data;
					});
		
	};
	
	var updateRecord = function (record){
		return $http.post('queries/update_record.php', record)
					.then(function(response){
						return response.data;
					});
		
	};
	
	var replaceRecord = function (record){
		return $http.post('queries/replace_record.php', record)
					.then(function(response){
						return response.data;
					});
		
	};
		
	return {
		hasRecord : hasRecord,
		getRecord : getRecord,
		selectRecord : selectRecord,
		insertRecord : insertRecord,
		deleteRecord : deleteRecord,
		updateRecord : updateRecord,
		replaceRecord : replaceRecord,
	};
};

var module = angular.module('planify.db', []);
module.factory('db', db);
}());