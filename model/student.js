(function(){
'use strict';

var config = function(localStorageServiceProvider){
	localStorageServiceProvider
    .setStorageType('sessionStorage');
};

var student = function($http, $cookieStore, localStorageService){
	var vm = {};
	
	var initStudent = function (data){
		vm['id'] = data['id'];
		vm['firstName'] = data['firstName'];
		vm['lastName'] = data['lastName'];
		vm['sequence'] = data['sequence'];
		vm['completed'] = data['completed'];
		
		localStorageService.set('id', data['id']);
		localStorageService.set('firstName', data['firstName']);
		localStorageService.set('lastName', data['lastName']);
		localStorageService.set('sequence', data['sequence']);
		localStorageService.set('completed', data['completed']);

	};
		
	return {
		initStudent : initStudent,
		getID : function(){return vm['id'];},
		getFirstName : function(){return vm['firstName'];},
		getLastName : function(){return vm['lastName'];},
		getSequence : function(){return vm['sequence'];},
		getCompleted : function(){return vm['completed'];}
	};
};

var module = angular.module('planify.student', ['ngCookies', 'LocalStorageModule']);
module.config(['localStorageServiceProvider', config]);
module.factory('student', ['$http', '$cookieStore', 'localStorageService', student]);
}());