(function(){
'use strict';

var config = function(localStorageServiceProvider){
	localStorageServiceProvider
    .setStorageType('sessionStorage');
};

var user = function($http, $cookieStore, localStorageService){

	var vm = {};
	
	var initUser = function (data){
		vm['username'] = data['username'];
		vm['admin'] = data['admin'];
		
		localStorageService.set('username', data['username']);
		localStorageService.set('admin', data['admin']);
	};
	
	return {
		initUser : initUser,
		getUsername : function(){return vm['username'];},
		isAdmin : function(){return vm['admin'];}
	};
};

var module = angular.module('planify.user', ['ngCookies', 'LocalStorageModule']);
module.config(['localStorageServiceProvider', config]);
module.factory('user', ['$http', '$cookieStore', 'localStorageService', user]);
}());