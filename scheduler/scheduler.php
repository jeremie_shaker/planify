<!--changed-->
<div id="left_space">



<h1 data-ng-init="init()"> Scheduler </h1>

<tabset>
<button class="btn btn-primary" ng-click="scheduler.addSchedule()">Add Semester</button>
    <tab ng-repeat="(index, schedule) in scheduler.schedules" heading="Semester {{index+1}}">
        <div class="btn-group">
            <label class="btn btn-primary" ng-model="schedule.Semester" btn-radio="'1'">Fall</label>
            <label class="btn btn-primary" ng-model="schedule.Semester" btn-radio="'2'">Winter</label>
            <label class="btn btn-primary" ng-model="schedule.Semester" btn-radio="'4'">Summer</label>
        </div>
        
        <!--changed-->  
<div class="row">

        <!--space between fall/winter/summer tabs and courses table -->
        <br>
  <div class="col-md-4">
        
        <!-- changed -->
        <div class = "row">
            <div class = " col-md-12">
        
        <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Courses</th>
            </tr>
            <tr ng-repeat="(i,todo) in scheduler.todos">
                <td><a ng-click="schedule.selectCourse(i)">{{todo}}</a></td>
            </tr>
        </table>
        </div>
        <!--changed -->
        <br>
        
        <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <tr>
                <th>Selected</th>
            </tr>
            <tr ng-repeat="(i,selectedCourses) in schedule.selectedCourses">
                <td><a ng-click="schedule.removeCourse(i)">{{selectedCourses}}</a></td>
            </tr>
        </table>
        </div>
        
        <!--changed-->
        <div ng-show="schedule.conflict" class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            {{schedule.conflict}}
        </div>
        <center>
        <button class="btn btn-success" ng-click="schedule.generateSchedule()">Generate</button>
        &nbsp <!-- spaces -->
        
        <button class="btn btn-info" ng-click="schedule.addPreference()">Add Preferences</button>
      <!-- changed -->
      </center>
      </div></div>
      <br>
      <div class="row">
      <div class="col-md-12">
        
        
        
        
    <div ng-repeat="(p,preference) in schedule.preferences">
        <!-- changed -->
        <div class="border_color">
            Time Start:
            <select ng-model="preference.timeStart">
                <option ng-repeat="timing in preference.timings" value="{{timing}}">{{timing}}</option>
            </select>
            Time End:
            <select ng-model="preference.timeEnd">
                <option ng-repeat="timing in preference.timings" value="{{timing}}">{{timing}}</option>
            </select>
            <br>
            Day:
            <label class="btn" type="checkbox" btn-radio="'M'" ng-model="preference.day">M</label>
            <label class="btn" type="checkbox" btn-radio="'T'" ng-model="preference.day">T</label>
            <label class="btn" type="checkbox" btn-radio="'W'" ng-model="preference.day">W</label>
            <label class="btn" type="checkbox" btn-radio="'J'" ng-model="preference.day">J</label>
            <label class="btn" type="checkbox" btn-radio="'F'" ng-model="preference.day">F</label>
            <button class="btn btn-danger" ng-click="schedule.removePreference(p)">Delete</button>
        </div>
    <br>
    </div>
        
        <!--changed-->
        
        </div></div>
        </div>
        
        
        
        <!--changed-->
        <div class="col-md-8">
        
        <div class="row">
         <div class="col-md-12">
            
            <center>
            <div class="table-responsive">
            <table class="table table-bordered" id="sections">
                <tr>
                    <td>Course ID</td>
                    <td>Lecture Section</td>
                    <td>Lecture Day(s)</td>
                    <td>Lecture Start</td>
                    <td>Lecture End</td>
                    <td>Lecture Location</td>
                    <td>Tutorial Section</td>
                    <td>Tutorial Day(s)</td>
                    <td>Tutorial Start</td>
                    <td>Tutorial End</td>
                    <td>Tutorial Location</td>
                    <td>Laboratory Section</td>
                    <td>Laboratory Day(s)</td>
                    <td>Laboratory Start</td>
                    <td>Laboratory End</td>
                    <td>Laboratory Location</td>
                </tr>
                <tr ng-repeat="section in schedule.selectedSections">
                    <td>{{section.CourseID}}</td>
                    <td>{{section.LecID}}</td>
                    <td>{{section.LecDay}}</td>
                    <td>{{section.LecStart}}</td>
                    <td>{{section.LecEnd}}</td>
                    <td>{{section.LecRoom}}</td>
                    <td>{{section.TutID}}</td>
                    <td>{{section.TutDay}}</td>
                    <td>{{section.TutStart}}</td>
                    <td>{{section.TutEnd}}</td>
                    <td>{{section.TutRoom}}</td>
                    <td>{{section.LabID}}</td>
                    <td>{{section.LabDay}}</td>
                    <td>{{section.LabStart}}</td>
                    <td>{{section.LabEnd}}</td>
                    <td>{{section.LabRoom}}</td>
                </tr>
            </table>
            </div>

        <!--changed -->
        <br>
            <div class="btn-group" dropdown is-open="status.isopen">
                <button type="button" class="btn btn-success dropdown-toggle" dropdown-toggle ng-disabled="disabled">
                Save <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a onClick ="$('#schedule').tableExport({type:'png',escape:'false'});">Schedule</a></li>
                    <li><a onClick ="$('#sections').tableExport({type:'png',escape:'false'});">Course List</a></li>
                </ul>
            </div>
            <!--button class="btn btn-success" onClick ="$('#schedule').tableExport({type:'png',escape:'false'});">Save</button-->
            <button class="btn btn-danger" ng-click="scheduler.deleteSchedule(index)">Delete</button>
            <button class="btn btn-warning" ng-click="schedule.clearSchedule()">Clear</button>
        </center>
        
        <!--changed-->
        <br> <!-- space between buttons and schedule -->
        </div></div>
        
        <div class="row">
        <div class="col-md-12">
        
        <center>
        <div class="table-responsive" id="s_space">
            <table class="table table-striped table-bordered schedule" id="schedule">
                    <tr>
                        <th>Time/Day</th>
                        <th class="th_size">Monday</th>
                        <th class="th_size">Tuesday</th>
                        <th class="th_size">Wednesday</th>
                        <th class="th_size">Thursday</th>
                        <th class="th_size">Friday</th>
                    </tr>
                    <tr ng-repeat="(key, timeslot) in schedule.time">
                        <td>{{key}}</td>
                        <td>{{timeslot.M}}</td>
                        <td>{{timeslot.T}}</td>
                        <td>{{timeslot.W}}</td>
                        <td>{{timeslot.J}}</td>
                        <td>{{timeslot.F}}</td>
                    </tr>
                </tr>
            </table>
            </div>
        </center>
            <!--changed-->
        </div></div>
        
        
        
        
        
        
        
        
        </div>
        
</div>
        
    </tab>
</tabset>



</div>
