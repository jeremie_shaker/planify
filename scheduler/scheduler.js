'use strict';

angular.module('planify.scheduler', ['ngRoute', 'ngCookies', 'LocalStorageModule'])

.config(['$routeProvider', 'localStorageServiceProvider', function($routeProvider, localStorageServiceProvider) {
  $routeProvider.when('/scheduler', {
    templateUrl: 'scheduler/scheduler.php',
    controller: 'SchedulerCtrl'
  });
  localStorageServiceProvider
    .setStorageType('sessionStorage');
}])

.controller('SchedulerCtrl', ['$scope', '$http', '$cookieStore', '$cacheFactory', 'localStorageService', function($scope, $http, $cookieStore, $cacheFactory, localStorageService){

    
    var DEFAULTSEQUENCE = ['COMP232', 'COMP248', 'ENGR201','ENGR213','S-ELECTIVE1','COMP249', 'ENGR233', 'SOEN228', 'SOEN287', 'S-ELECTIVE2', 'COMP348', 'COMP352', 'ENCS282', 'ENGR202', 'G-ELECTIVE1', 'COMP346', 'ELEC275', 'ENGR371', 'SOEN331', 'SOEN341', 'COMP335', 'SOEN342', 'SOEN343', 'SOEN384', 'ENGR391', 'SOEN344', 'SOEN345', 'SOEN357', 'SOEN390', 'SOEN-ELECTIVE1', 'SOEN490(1)', 'ENGR301', 'SOEN321', 'T-ELECTIVE1', 'T-ELECTIVE2', 'SOEN385', 'ENGR392', 'SOEN490(2)', 'SOEN-ELECTIVE2', 'SOEN-ELECTIVE3'];

    $scope.init = function(){
        $scope.$parent.hideMenu = false;
        //$scope.$parent.reloadSession();
        $scope.scheduler = new Scheduler();
        $scope.sections = {1: [], 2: [], 3: [],4: []};
        parseSections();
        if(localStorageService.get('sequence')){
            localStorageService.get('sequence').forEach(function(course){
                $scope.scheduler.todos.push(course.courseID);
            });
            localStorageService.get('completed').forEach(function(course){
                $scope.scheduler.todos.remove(course);
            });
        }
        else{
            $scope.scheduler.todos = DEFAULTSEQUENCE;
        }
        
        $scope.scheduler.selectAllCourses();
        
    };

    var addSchedule = function(){
        if(this.schedules.length < 8){
            this.schedules.push(new Schedule());
        }  
        else{
            console.log("Reached max num of schedules.");
        }
    };

    var deleteSchedule = function(index){
        while(typeof this.schedules[index].selectedCourses[0] != "undefined"){
            this.schedules[index].removeCourse(0);
        }
        this.schedules.splice(index, 1);
    };

    var Scheduler = function(){
        this.schedules = [];
        this.addSchedule = addSchedule;
        this.deleteSchedule = deleteSchedule;
        this.todos = [];
        this.selectAllCourses = selectAllCourses;
    };

    var Preference = function(){
        this.day = 'M';
        this.timeStart = '08:15';
        this.timeEnd = '23:00';
        this.timings = timeRangeToArray("08:15","23:00");
    };

    var addPreference = function(){
        this.preferences.push(new Preference());
    };

    var removePreference = function(i){
        this.preferences.splice(i,1);
    }

    var selectCourse = function(i){
        if(this.selectedCourses.length < 5){
            this.selectedCourses.push($scope.scheduler.todos.splice(i,1)[0]);
        }
        else{
            this.error = 'Cannot add more than 5 courses';
        }
    };

    var removeCourse = function(i){
        $scope.scheduler.todos.push(this.selectedCourses.splice(i,1)[0]);
    };

    var selectSection = function(section){
        var timeBlocks = convertTime(section)
        for(var i=0; i < timeBlocks.length; i++){
            this.time[timeBlocks[i][0]][timeBlocks[i][1]] = section.CourseID;
        }
        this.selectedSections.push(section);
    };

    var removeSection = function(section){
        for(var i in this.selectedSections){
            if(this.selectedSections[i].CourseID.trim() == section.CourseID.trim()){
                this.selectedSections.splice(i,1);
                var timeBlocks = convertTime(section);
                for(var j = 0; j < timeBlocks.length; j++){
                    this.time[timeBlocks[j][0]][timeBlocks[j][1]] = "";
                }
                console.log("Section for "+section.CourseID+" was successfully removed");
                return;
            }
        }
        console.log("Section for "+section.CourseID+" wasn't selected to start with");
    };

    var checkConflicts = function(section){
        var timeBlocks = convertTime(section)
        for(var i=0; i < timeBlocks.length; i++){
            if(typeof this.time[timeBlocks[i][0]] == "undefined"){
                this.conflict = 'Tried inserting time block at '+timeBlocks[i][0]+" on "+timeBlocks[i][1];
            }
            if(this.time[timeBlocks[i][0]][timeBlocks[i][1]] != ""){
                return true;
            }
        }
        return false;
    };

    //Generates a new schedule object part of the schedules array
    var Schedule = function(){
        this.selectedCourses = [];
        this.selectedSections = [];
        this.Semester = '1';
        this.time = {};
        this.selectCourse = selectCourse;
        this.removeCourse = removeCourse;
        this.selectSection = selectSection;
        this.removeSection = removeSection;
        this.generateSchedule = generateSchedule;
        this.clearSchedule = clearSchedule;
        this.trimSchedule = trimSchedule;
        this.checkConflicts = checkConflicts;
        this.preferences = [];
        this.addPreference = addPreference;
        this.removePreference = removePreference;
        this.conflict = '';
        var timeArray = timeRangeToArray("08:15","23:00");
        for(var i =0; i < timeArray.length; i++){
            //Instantiated the second part of the 2D array, the day. We do time->day and not the opposite for the ng-repeat.
                this.time[timeArray[i]] = {};
                //Looping through 5 first days, mon-fri. can expand to sunday easily. made D = sunday. idk don't judge.
                for(var k=0; k <= 4; k++){
                    var day = "M";
                    switch(k){
                        case 0: day ="M"; break;
                        case 1: day = "T"; break;
                        case 2: day = "W"; break;
                        case 3: day = "J"; break;
                        case 4: day = "F"; break;
                        case 5: day = "S"; break;
                        case 6: day = "D"; break;
                    }
                    //This is purely for test sake. The instantiated variables will be binded during schedule generation.
                    this.time[timeArray[i]][day] = "";
                    
                }

        }
    };

    var selectAllCourses = function(){
        var i = -1;
        while(this.todos.length > 0){
            i++;
            var j = Math.floor(i/5);
            if(this.schedules[j]){
                this.schedules[j].selectCourse(0);
            }
            else{
                this.schedules.push(new Schedule());
                this.schedules[j].selectCourse(0);
            }
        }
    }

    function timeRangeToArray(start, end){
        var time = start;
        var timeBlocks = [];
        if(start.length != 5 || start.length !=5){
            console.log("Could not convert time to array, invalid format.");
            return [];
        }
        if(start.timeStringToInt() >= end.timeStringToInt()){
            console.log("Could not convert time to array, end must be past the start");
            return [];
        }
        while(time != end){
            timeBlocks.push(time);

            var minutes = parseInt(time.substring(3,5));
            var hours = parseInt(time.substring(0,2));
            minutes += 5;
            if(minutes == 60){
                minutes = 0;
                hours++;
            }
            if(minutes < 10){
                minutes = "0"+minutes
            }
            if(hours < 10){
                hours = "0"+hours;
            }
            time = hours+":"+minutes;
            if(time == end){
                timeBlocks.push(time);
                break;
            }
        }
        return timeBlocks;

    }

    String.prototype.timeStringToInt = function(){
        if(this.length < 5){
            console.log("Invalid timeStringToInt format "+this);
            return false;
        }
        return parseInt(this.substring(0,5).replace(":",""));
    };

    Number.prototype.intTimeToString = function(){
        if(this<1000){
            if(this<100){
                return "00:"+this;
            }
            else{
                return "0"+this.toString().insert(1,":");
            }
        }
        else{
            return this.toString().insert(2,":");
        }
    };

    //Converts whatever time format is received from the DB into an array of strings.
    function convertTime(section){
        var timeBlocks = [];
        if(section.LecDay){
            var lecDays = section.LecDay.split('');
            var start = section.LecStart.substring(0,5);
            var end = section.LecEnd.substring(0,5);
            lecDays.forEach(function(day){
                timeRangeToArray(start,end).forEach(function(time){
                    timeBlocks.push([time,day])
                });
            });
                
        }
        if(section.TutDay){
            var tutDays = section.TutDay.split('');
            var start = section.TutStart.substring(0,5);
            var end = section.TutEnd.substring(0,5);
            tutDays.forEach(function(day){
               timeRangeToArray(start,end).forEach(function(time){
                    timeBlocks.push([time,day])
                });
            });
        }
        if(section.LabDay){
            var labDays = section.LabDay.split('');
            var start = section.LabStart.substring(0,5);
            var end = section.LabEnd.substring(0,5);
            labDays.forEach(function(day){
                timeRangeToArray(start,end).forEach(function(time){
                    timeBlocks.push([time,day])
                });
            });
        }
        //Example of a possible return value.
        return timeBlocks;
    }
    var parseSections = function(){
       localStorageService.get('masterList').forEach(function(section){
            if(section.Elective == "general"){
                if($scope.sections[section.Semester]['G-ELECTIVE']){
                    $scope.sections[section.Semester]['G-ELECTIVE'].push(section);   
                }
                else{
                    $scope.sections[section.Semester]['G-ELECTIVE'] = [];
                    $scope.sections[section.Semester]['G-ELECTIVE'].push(section);
                }
            }

            if(section.Elective == "science"){
                if($scope.sections[section.Semester]['S-ELECTIVE']){
                    $scope.sections[section.Semester]['S-ELECTIVE'].push(section);   
                }
                else{
                    $scope.sections[section.Semester]['S-ELECTIVE'] = [];
                    $scope.sections[section.Semester]['S-ELECTIVE'].push(section);
                }
            }
            //Hard coded for capstone
			//you had done section.Semester = '3'
			//instead of section.Semester == '3'
			//you were assigning each section.Semester to 3! lol
			//I fixed it for you, should be good now
            if(section.Semester == '3'){
                if($scope.sections['1'][section.CourseID]){
                    $scope.sections['1'][section.CourseID].push(section);
                }
                else{
                    $scope.sections['1'][section.CourseID] = [];
                    $scope.sections['1'][section.CourseID].push(section);
                }
                if($scope.sections['2'][section.CourseID]){
                    $scope.sections['2'][section.CourseID].push(section);
                }
                else{
                    $scope.sections['2'][section.CourseID] = [];
                    $scope.sections['2'][section.CourseID].push(section);
                }
            }
            else if($scope.sections[section.Semester][section.CourseID]){
                $scope.sections[section.Semester][section.CourseID].push(section);
            }
            else{
                $scope.sections[section.Semester][section.CourseID] = [];
                $scope.sections[section.Semester][section.CourseID].push(section);
            }
        });
    };

    var clearSchedule = function(){
        var timeArray = timeRangeToArray("08:15","23:00");
        for(var i =0; i < timeArray.length; i++){
            //Instantiated the second part of the 2D array, the day. We do time->day and not the opposite for the ng-repeat.
                this.time[timeArray[i]] = {};
                //Looping through 5 first days, mon-fri. can expand to sunday easily. made D = sunday. idk don't judge.
                for(var k=0; k <= 4; k++){
                    var day = "M";
                    switch(k){
                        case 0: day ="M"; break;
                        case 1: day = "T"; break;
                        case 2: day = "W"; break;
                        case 3: day = "J"; break;
                        case 4: day = "F"; break;
                        case 5: day = "S"; break;
                        case 6: day = "D"; break;
                    }
                    //This is purely for test sake. The instantiated variables will be binded during schedule generation.
                    this.time[timeArray[i]][day] = "";
                    
                }

        }
        this.selectedSections = [];
        this.conflict = '';
    }

    var trimSchedule = function(){
        var start = this.selectedSections[0].LecStart.timeStringToInt();
        var end = this.selectedSections[0].LecEnd.timeStringToInt();
        for(var i=0 ; i<this.selectedSections.length ; i++){
            if(this.selectedSections[i].LecStart != null){
                var lecStart=this.selectedSections[i].LecStart.timeStringToInt();
                if(lecStart < start && lecStart != 0){
                    start = lecStart;
                }
            }
            if(this.selectedSections[i].TutStart != null){
                var tutStart=this.selectedSections[i].TutStart.timeStringToInt();
                if(tutStart < start && tutStart != 0){
                    start = tutStart;
                }
            }
            if(this.selectedSections[i].LabStart != null){
                var labStart=this.selectedSections[i].LabStart.timeStringToInt();
                if(labStart < start && labStart != 0){
                    start = labStart;
                }
            }
            if(this.selectedSections[i].LecEnd != null){
                var lecEnd=this.selectedSections[i].LecEnd.timeStringToInt();
                if(lecEnd > end){
                    end = lecEnd;
                }
            }
            if(this.selectedSections[i].TutEnd != null){
                var tutEnd=this.selectedSections[i].TutEnd.timeStringToInt();
                if(tutEnd > end){
                    end = tutEnd;
                }
            }
            if(this.selectedSections[i].LabEnd != null){
                var labEnd=this.selectedSections[i].LabEnd.timeStringToInt();
                if(labEnd > end){
                    end = labEnd;
                }
            }
        }
        start = start.intTimeToString();
        end = end.intTimeToString();
        console.log("Earliest day starts at " +start);
        console.log("Latest day ends at "+end);
        for(var i in this.time){
            if(i != start){
                delete this.time[i];
            }
            else{
                break;
            }
        }
        var deleteRest;
        for(var i in this.time){
            if(i == end){
                deleteRest = true;
            }
            else if(deleteRest){
                delete this.time[i];
            }
        }

    }
    //Algorithm used to make the schedule
    var generateSchedule = function(){
        this.clearSchedule();
        var courseCategory = this.selectedCourses.slice(0);
        var tracker = [0,0,0,0,0];
        //Taking care of electives to be renamed.
        for(var index=0; index<courseCategory.length; index++){
            if(courseCategory[index].indexOf('ELECTIVE') > -1){
                courseCategory[index] = courseCategory[index].substring(0,courseCategory[index].length-1);
            }
            //And capstone
            if(courseCategory[index].indexOf('SOEN490') > -1){
                courseCategory[index] = 'SOEN490';
            }
        }
        //Blocking off preferences
        for(var i =0; i< this.preferences.length ; i++){
            var blockedTimes = timeRangeToArray(this.preferences[i].timeStart,this.preferences[i].timeEnd);
            for(var j =0; j < blockedTimes.length; j++){
                console.log("Blocking "+blockedTimes[j]+" on "+this.preferences[i].day);
                this.time[blockedTimes[j]][this.preferences[i].day] = "-";
            }
        }
        //Course algorithm starts here
        for(var i=0; i<5; i++){
            if(typeof courseCategory[i] === 'undefined'){
                return true;
            }
            if (typeof $scope.sections[this.Semester][courseCategory[i]] === 'undefined') {
                this.conflict = "Course " + courseCategory[i] + " is not offered this Semester. Schedule generation terminated.";
                return false;
            }
            for(; tracker[i]<$scope.sections[this.Semester][courseCategory[i]].length; tracker[i]++){
                console.log("Course "+i+" index "+tracker[i]+" out of "+($scope.sections[this.Semester][courseCategory[i]].length-1));
                if(this.checkConflicts($scope.sections[this.Semester][courseCategory[i]][tracker[i]])){
                    if(tracker[i] <  $scope.sections[this.Semester][courseCategory[i]].length-1){
                        continue;
                    }
                    tracker[i] = 0;
                    if(i==0){
                        console.log("Could not generate schedule because "+courseCategory[i]+" conflicts with preferences.");
                        this.conflict = "Could not generate schedule because "+courseCategory[i]+" conflicts with preferences.";
                        return false;
                    }
                    i--;
                    console.log("Tried all possibilities for "+courseCategory[i+1] + " removing "+courseCategory[i]+ " index "+ tracker[i]);
                    this.removeSection($scope.sections[this.Semester][courseCategory[i]][tracker[i]]);
                }
                else{
                    console.log("Selected section for "+courseCategory[i] + " index "+tracker[i]);
                    this.selectSection($scope.sections[this.Semester][courseCategory[i]][tracker[i]]);
                    break;
                }
                if(tracker[i]+1 == $scope.sections[this.Semester][courseCategory[i]].length){
                    this.conflict = "Could not generate schedule due to "+courseCategory[i]+" conflicting with "+courseCategory[i+1];
                    return false;
                }
            }
        }
        //this.trimSchedule();
    }

    Array.prototype.remove = function() {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };

    String.prototype.insert = function (index, string) {
      if (index > 0)
        return this.substring(0, index) + string + this.substring(index, this.length);
      else
        return string + this;
    };

    
}]);