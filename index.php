<!DOCTYPE html>
<?php
    session_start();
?>
<!--[if lt IE 7]>      <html lang="en" ng-app="planify" ng-controller="PlanifyCtrl" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" ng-app="planify" ng-controller="PlanifyCtrl" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" ng-app="planify" ng-controller="PlanifyCtrl" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" ng-app="planify" ng-controller="PlanifyCtrl" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Planify</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bower_components/html5-boilerplate/css/normalize.css">
    <link rel="stylesheet" href="bower_components/html5-boilerplate/css/main.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">
	<link rel="stylesheet" href="bower_components/ui-grid/release/ui-grid-unstable.css"/>
	
	<!--ADDED-->
	<link rel="icon" href="images/icon_browser.png" type="image/x-icon" />
	
	<script src="bower_components/html5-boilerplate/js/vendor/modernizr-2.6.2.min.js"></script> 
</head>
<body>

<div id="bgImage">


    <div class="col-lg-12 col-xs-12" data-ng-init="init()" ng-show="!hideMenu">       
                <!--  ref:  http://getbootstrap.com/components/#navbar  -->
                
        <nav class="navbar ">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="color: #2f546a;">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#/dashboard">Planify</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="#/scheduler">Scheduler</a></li> <!--Dashboard --> 
                        <li class="dropdown">
                        <a href="#/courselist" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Course List <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                        </ul>
                        </li>
                    </ul>
                    <div class="nav navbar-nav navbar-right btn-group" dropdown ng-show="username">
                        <button ng-click="destroySession()" type="button" class="btn btn-danger">Log Out</button>
                        <!-- changed  took off-->
                    </div>
                    <div class="nav navbar-nav navbar-right btn-group" dropdown ng-hide="username">
                        <a href="#/" class="btn btn-success">Log In</a>
                    </div>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>  
    </div>
    <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

	
	
    <div ng-view></div>


    <footer class="container navbar navbar-default navbar-responsive-bottom col-lg-12 col-xs-12" id="f_design">
        <!--ADDED-->
	<img src="images/icon_navbar1.png" style="height:90%"/>
		<div class="navbar-text pull-right" >
            <div class="footer" id="footer-links">
                <a href="#/">Privacy Statement</a>
                <a href="contactUs.php">Contact Us</a>
            </div>
            <div class="footer" id="browser-icon">
                <p><i>Planify 2015 © </i> Designed for use with 
                <a href="http://www.google.com/chrome/"><img src="images/chrome-logo.png"/></a></p>
                <!-- IMAGE REFERENCE: http://www.1stwebdesigner.com/tutorials/best-written-explained-logo-design-tutorials/ -->
            </div>
        </div>  
    </footer>
    <!-- In production use:
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/x.x.x/angular.min.js"></script>
    -->
    <script src="bower_components/jquery/dist/jquery.js"></script>
	<script type="text/javascript" src="tableExport/tableExport.js"></script>
    <script type="text/javascript" src="tableExport/jquery.base64.js"></script>
    <script type="text/javascript" src="tableExport/html2canvas.js"></script>
    <script type="text/javascript" src="tableExport/jspdf/libs/sprintf.js"></script>
    <script type="text/javascript" src="tableExport/jspdf/jspdf.js"></script>
    <script type="text/javascript" src="tableExport/jspdf/libs/base64.js"></script>
    <script src="bower_components/angular/angular.js"></script>
    <script src="bower_components/angular-route/angular-route.js"></script>
	<script src="bower_components/angular-mocks/angular-mocks.js"></script>
	<script src="bower_components/angular-cookies/angular-cookies.js"></script>
    <script src="bower_components/angular-bootstrap/ui-bootstrap.min.js"></script>
    <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
	<script src="bower_components/ui-grid/release/ui-grid-unstable.js"></script>
	<script src="bower_components/angular-local-storage/angular-local-storage.js"></script>
    <script src="app.js"></script>
	<script src="model/db.js"></script>
	<script src="model/user.js"></script>
	<script src="model/student.js"></script>
    <script src="login/login.js"></script>
    <script src="dashboard/dashboard.js"></script>
    <script src="courselist/courselist.js"></script>
	<script src="test/recordtest.js"></script>
    <script src="scheduler/scheduler.js"></script>
    <script src="components/version/version.js"></script>
    <script src="components/version/version-directive.js"></script>
    <script src="components/version/interpolate-filter.js"></script>
	
</div>
</body>
</html>
