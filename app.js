'use strict';

// Declare app level module which depends on views, and components
angular.module('planify', [
    'ngRoute',
	'ngCookies',
    'ui.bootstrap',
	'planify.db',
	'planify.user',
	'planify.student',
    'planify.login',
    'planify.dashboard',
    'planify.courselist',
    'planify.scheduler',
	'planify.recordtest',
	'LocalStorageModule',
    'planify.version'
])
.config(['$routeProvider', 'localStorageServiceProvider', function($routeProvider, localStorageServiceProvider) {
    $routeProvider.otherwise({redirectTo: '/'});
	localStorageServiceProvider
    .setStorageType('sessionStorage');
}])
.controller('PlanifyCtrl', ['$scope', '$http', '$location', 'db', 'user', 'student', '$cacheFactory', '$cookieStore', 'localStorageService', function($scope, $http, $location, db, user, student, $cacheFactory, $cookieStore, localStorageService) {
	
	$scope.cache = $cacheFactory('cacheId');

    $scope.session = {};
	
	$scope.username = {};

    $scope.hideMenu = false;

    $scope.init = function(){
        $scope.reloadSession();
        queryAll();
        queryAllCourses();
		querySequences();

    };
	
    //Re-syncs session with the PHP session
    $scope.reloadSession = function(){
        $http.get('session/session.php')
            .success(function(data, status, headers, config){
				$scope.session.admin = data.admin;
				$scope.session.username = data.username;
                $scope.session.id = data.id;
                $scope.session.fname = data.fname;
                $scope.session.lname = data.lname;
                $scope.session.completed = data.completed;
                $scope.session.sequence = data.sequence;
            })
            .error(function(data, status, headers, config){
                $scope.destroySession();
                console.log("Unable to update session. GET request has failed on session.php");
            });
    };

    //Imitation of a session destroy on PHP
    $scope.destroySession = function(){
        $http.get('session/logout.php')
            .success(function(data, status, headers, config){
                $scope.session = {};
                console.log("Successfully logged out!");
                $location.path('/');
				localStorageService.clearAll();
				$scope.init();
            })
            .error(function(data, status, headers, config){

                console.log("Error logging out");

            }
        )
    };
	
    var queryAll = function(){
        
        var record = {
            'table' : 'section',
            'columns' : ['SectionID', 'CourseID', 'AcademicPeriod', 'Semester', 'Capacity', 'RealCapacity', 'EngCredited',
						'LecID', 'LecTA', 'LecRoom', 'LecDay', 'LecStart', 'LecEnd',
						'TutID', 'TutTA', 'TutRoom', 'TutDay', 'TutStart', 'TutEnd',
						'LabID', 'LabTA', 'LabRoom', 'LabDay', 'LabStart', 'LabEnd', 'Elective'],
			'join' : {'[>]course' : {'CourseID' : 'ID'}},
            'where' : ''
        };
        
        db.selectRecord(record)
            .then(function(data){
				localStorageService.set('masterList', data);
                
            }, function(data){
                console.log("Failed to load all sections!");
            });
    };

	var queryAllCourses = function(){
		
		var record = {
			'table' : 'course',
			'join' : '',
			'columns' : '*',
			'where' : ''
		};
		
		db.selectRecord(record)
            .then(function(data){
					localStorageService.set('courseList', data);
			}, function(data){
                console.log("Failed to load courses!")
            });
	};
	
	var querySequences = function(){
		
		var record = {
			'table' : 'default_sequence',
			'join' : '',
            'columns' : '*',
			'where' : ''
		};
		
		db.selectRecord(record)
            .then(function(data){
					localStorageService.set('sequenceList', data);
			}, function(data){
                console.log("Failed to load sequences!")
            });
	};

}]);
