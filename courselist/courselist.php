<div ng-controller="CourseListCtrl">
	<style>
	p {
	   margin-top:10px;
	   margin-bottom:10px;
	}
	</style>
	<h1>Course List</h1>
	<div>
		<p> 
			Last Cell Edit: 
			<div ng-repeat= "(item, value) in cellEdits">
			{{item}} : {{value}}
			</div>
		</p>
		<p>Current page: {{ gridApi.pagination.getPage() }} of {{ gridApi.pagination.getTotalPages() }}</p>
	<div class="btn-group">
		<label class="btn btn-success" ng-click="gridApi.pagination.previousPage()">Previous Page</label>
		<label class="btn btn-success" ng-click="gridApi.pagination.nextPage()">Next Page</label>
	</div>
	<div class="btn-group">
		<label class="btn btn-primary" ng-model="radioModel" ng-click="displayCourses()" btn-radio="'Courses'">Courses</label>
		<label class="btn btn-primary" ng-model="radioModel" ng-click="displayLectures()" btn-radio="'Lectures'">Lectures</label>
		<label class="btn btn-primary" ng-model="radioModel" ng-click="displayTutorials()" btn-radio="'Tutorials'">Tutorials</label>
		<label class="btn btn-primary" ng-model="radioModel" ng-click="displayLabs()" btn-radio="'Labs'">Labs</label>
	</div>
	<label class="btn btn-success" ng-model="radioModel" ng-hide="loaded">Loading data please wait...</label>
	<p>
	<div id="coursetable" ui-grid="gridOptions" ui-grid-pagination ui-grid-edit ui-grid-row-edit ui-grid-cellNav ui-grid-auto-resize ui-grid-resize-columns></div>
	</p>
	</div>