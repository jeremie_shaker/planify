(function(){
'use strict';

var config = function($routeProvider, localStorageServiceProvider){
    $routeProvider.when('/courselist', {
    templateUrl: 'courselist/courselist.php',
    controller: 'CourseListCtrl'
  });
  localStorageServiceProvider
    .setStorageType('sessionStorage');
};

var CourseListCtrl = function ($scope, $http, $q, $timeout, db, localStorageService){
    var vm = this;
    
    var table = {};
    var targetColumn = {};
    var oldValue = {};
    var newValue = {};
    
    var courseColumns = [
            {name: 'ID' },
            {name: 'Name' },
            {name: 'Credit' },
            {name: 'Elective'}
    ];
    
    var lectureColumns = [
            {name: 'SectionID' },
            {name: 'CourseID' },
            {name: 'Semester' },
            {name: 'Capacity'},
            {name: 'EngCredited'},
            {name: 'LecID'},
            {name: 'LecTA'},
            {name: 'LecRoom'},
            {name: 'LecDay'},
            {name: 'LecStart'},
            {name: 'LecEnd'}
    ];
    
    var tutorialColumns = [
            {name: 'SectionID' },
            {name: 'CourseID' },
            {name: 'Semester' },
            {name: 'Capacity'},
            {name: 'EngCredited'},
            {name: 'TutID'},
            {name: 'TutTA'},
            {name: 'TutRoom'},
            {name: 'TutDay'},
            {name: 'TutStart'},
            {name: 'TutEnd'}
    ];
    
    var labColumns = [
            {name: 'SectionID' },
            {name: 'CourseID' },
            {name: 'Semester' },
            {name: 'Capacity'},
            {name: 'EngCredited'},
            {name: 'LabID'},
            {name: 'LabTA'},
            {name: 'LabRoom'},
            {name: 'LabDay'},
            {name: 'LabStart'},
            {name: 'LabEnd'}
    ];
    
    var queryAllCourses = function(){
        
        var record = {
            'table' : 'course',
			'join' : '',
            'columns' : '*',
            'where' : ''
        };
        
        db.selectRecord(record)
            .then(function(data){
                $scope.courseData = data;
                
            }, function(data){
                console.log("Failed to POST query!");
            });
    };
    
    var queryAllLectures = function(){

        var record = {
            'table' : 'section',
			'join' : '',
            'columns' : ['SectionID', 'CourseID', 'Semester', 'Capacity', 'EngCredited', 'LecID', 'LecTA', 'LecRoom', 'LecDay', 'LecStart', 'LecEnd'],
            'where' : ''
        };
        
        db.selectRecord(record)
            .then(function(data){
                $scope.lectureData = data;
                
            }, function(data){
                console.log("Failed to POST query!");
            });
    };
    
    var queryAllTutorials = function(){
        
        var record = {
            'table' : 'section',
			'join' : '',
            'columns' : ['SectionID', 'CourseID', 'Semester', 'Capacity', 'EngCredited', 'TutID', 'TutTA', 'TutRoom', 'TutDay', 'TutStart', 'TutEnd'],
            'where' : ''
        };
        
        db.selectRecord(record)
            .then(function(data){
                $scope.tutorialData = data;
                
            }, function(data){
                console.log("Failed to POST query!");
            });
    };
    
    var queryAllLabs = function(){
        
        var record = {
            'table' : 'section',
			'join' : '',
            'columns' : ['SectionID', 'CourseID', 'Semester', 'Capacity', 'EngCredited', 'LabID', 'LabTA', 'LabRoom', 'LabDay', 'LabStart', 'LabEnd'],
            'where' : ''
        };
        
        db.selectRecord(record)
            .then(function(data){
                $scope.labData = data;
                $scope.loaded = true;
                
            }, function(data){
                console.log("Failed to POST query!");
            });
    };

    $scope.queryAllCourses;
    $scope.queryAllLectures;
    $scope.queryAllTutorials;
    $scope.queryAllLabs;
    
    $scope.displayCourses = function(){
        vm.table = 'course';
        $scope.gridOptions.columnDefs = courseColumns;
        $scope.gridOptions.data = $scope.courseData;
    };
    
    $scope.displayLectures = function(){
        vm.table = 'section';
        $scope.gridOptions.columnDefs = lectureColumns;
        $scope.gridOptions.data = $scope.lectureData;
    };
    
    $scope.displayTutorials = function(){
        vm.table = 'section';
        $scope.gridOptions.columnDefs = tutorialColumns;
        $scope.gridOptions.data = $scope.tutorialData;
    };
    
    $scope.displayLabs = function(){
        vm.table = 'section';
        $scope.gridOptions.columnDefs = labColumns;
        $scope.gridOptions.data = $scope.labData;
    };
    
    $scope.fetchData = function(){
        queryAllCourses();
        queryAllLectures();
        queryAllTutorials();
        queryAllLabs();
    };
    
    var delayPromise = function(rowEntity) {
        var deferred = $q.defer();
        
        var tempRow = {};
        angular.copy(rowEntity, tempRow);
        tempRow[vm.targetColumn] = vm.oldValue;
        
        var data = {};
        data[vm.targetColumn] = vm.newValue;
        
        var record = { 
            'table' : vm.table,
            'data' :  data,
            'where' : {'AND' : tempRow}
        };
        
        db.updateRecord(record)
        .then(function(data) {
            console.log("Saving...");
            console.log("Number of rows affected" + data);
            deferred.resolve();
        }, function(data) {
            console.log("Failed to POST save!");
            deferred.reject();
        });
        
        return deferred.promise;
    };
    
    var firstKey = function (data) {
        for (var e in data) return e;
    };
    
    var catchRow = function(rowEntity, colDef, newValue, oldValue){ 
        vm.oldValue = oldValue;
        vm.newValue = newValue;
        vm.targetColumn = colDef['name'];
        
        var key = firstKey(rowEntity);
        $scope.cellEdits = { 'ID' : rowEntity[key], 'Column' : colDef['name'], 'New Value' : newValue, 'Old Value' : oldValue};
    };
    
    var saveRow = function(rowEntity) {
        var promise = delayPromise(rowEntity);
        $scope.gridApi.rowEdit.setSavePromise(rowEntity, promise);
    };
     
    $scope.addRecord = function(id, name, credit, elective) {
        

    };
    
    //Initialize data
    queryAllCourses();
    queryAllLectures();
    queryAllTutorials();
    queryAllLabs();
    
    $scope.gridOptions = {
        enableFiltering: true,
        enableCellEdit : localStorageService.get('admin'),
        enablePaginationControls: false,
        paginationPageSize: 100,
        flatEntityAccess : true
    };
    
    $scope.gridOptions.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;
        $scope.gridApi.edit.on.afterCellEdit($scope, catchRow);
        $scope.gridApi.rowEdit.on.saveRow($scope, saveRow);
    };
  
};

var app = angular.module('planify.courselist', ['ngRoute', 'LocalStorageModule', 'planify.db', 'ui.grid', 'ui.grid.pagination', 'ui.grid.autoResize', 'ui.grid.resizeColumns', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.cellNav']);
app.config(['$routeProvider', 'localStorageServiceProvider', config]);
app.controller('CourseListCtrl', [ '$scope', '$http', '$q', '$timeout', 'db', 'localStorageService', CourseListCtrl]);
}());
