<?php
require_once '../db_connect.php';
session_start();

    $postdata = json_decode(file_get_contents("php://input"), true);
    $username = $postdata['username'];
    $password = $postdata['password'];
	
	//verify if data is received
	if($username != null && $password != null){
		
		//set up query conditions
		$table = 'user';
		
		$join = [
		'[>]student' => 'Username'
		];
		
		$columns = [
		'user.Username',
		'user.Password',
		'user.Administrator',
		'student.ID',
		'student.FirstName',
		'student.LastName'
		];
		
		$where = [
		'user.Username' => $username,
		'LIMIT' => 1
		];
		
		//query database for student info based on user id
		$result = $database->select($table, $join, $columns, $where);
		
		if($result != false){
			echo false;
			
			$responseData = [];
			
			if(isset($result[0]['ID'])){
				//query completed + sequence courses
				$result_completed = $database->select('completed', 'CourseID', ['StudentID' => $result[0]['ID']]);
				$result_sequence = $database->select('student_sequence', ['[>]course' => ['courseID' => 'ID']], ['courseID', 'Name', 'Credit', 'Year', 'Semester'],  ['StudentID' => $result[0]['ID']]);
				
				$responseData = [
					'username' => $result[0]['Username'],
					'admin' => $result[0]['Administrator'],
					'id' => $result[0]['ID'],
					'firstName' => $result[0]['FirstName'],
					'lastName' => $result[0]['LastName'],
					'completed' => $result_completed,
					'sequence' => $result_sequence
				];
			}
			else{
				
				$responseData = [
					'username' => $result[0]['Username'],
					'admin' => $result[0]['Administrator']
				];
				
			}
			
			//match form password against database hashed password
			$check = password_verify($password, $result[0]['Password']);
			
			//if everything checks out add user/student info to session and echo true, else echo false
			if($username == $result[0]['Username'] && $check){
				$_SESSION['username'] = $result[0]['Username'];
				$_SESSION['admin'] = $result[0]['Administrator'];
				
				//$_SESSION['sequence'] = array('COMP232', 'COMP248', 'ENGR201','ENGR213', 'S-ELECTIVE1','COMP249', 'ENGR233', 'SOEN228', 'SOEN287', 'S-ELECTIVE2', 'COMP348', 'COMP352', 'ENCS282', 'ENGR202', 'G-ELECTIVE', 'COMP346', 'ELEC275', 'ENGR371', 'SOEN331', 'SOEN341', 'COMP335', 'SOEN342', 'SOEN343', 'SOEN384', 'ENGR391', 'SOEN344', 'SOEN345', 'SOEN357', 'SOEN390', 'SOEN-ELECTIVE1', 'SOEN490(1)', 'ENGR301', 'SOEN321', 'T-ELECTIVE1', 'T-ELECTIVE2', 'SOEN385', 'ENGR392', 'SOEN490(2)', 'SOEN-ELECTIVE2', 'SOEN-ELECTIVE3');
				echo json_encode($responseData);	
			}
		}
		else{
			session_destroy();
			echo false;
		}
	}
	
?>