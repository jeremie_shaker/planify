

<?php

    include_once '../db_connect.php';

    session_start();
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);
    $id = $request->id;
    $password = $request->password;

    //Ihcene from here on, you need to query the database. Find a match for $id and $password, and 
    //then set the session variables to whatever you retrieved from the database
    //Once the session variables are set, return one of the two responses below with an echo.
    if($id === "abc"){
        $_SESSION['id'] = $id;
        $_SESSION['fname'] = 'Jeremie';
        $_SESSION['lname'] = 'Shaker';
        $_SESSION['completed'] = array('COMP232', 'COMP248', 'ENGR201');
        $_SESSION['todo'] = array('SOEN341', 'SOEN331');
        $_SESSION['sequence'] = array('COMP232', 'COMP248', 'ENGR201','ENGR213', 'S-ELECTIVE1','COMP249', 'ENGR233', 'SOEN228', 'SOEN287', 'S-ELECTIVE2', 'COMP348', 'COMP352', 'ENCS282', 'ENGR202', 'G-ELECTIVE', 'COMP346', 'ELEC275', 'ENGR371', 'SOEN331', 'SOEN341', 'COMP335', 'SOEN342', 'SOEN343', 'SOEN384', 'ENGR391', 'SOEN344', 'SOEN345', 'SOEN357', 'SOEN390', 'SOEN-ELECTIVE1', 'SOEN490(1)', 'ENGR301', 'SOEN321', 'T-ELECTIVE1', 'T-ELECTIVE2', 'SOEN385', 'ENGR392', 'SOEN490(2)', 'SOEN-ELECTIVE2', 'SOEN-ELECTIVE3');
        echo '{"success": true}';
        exit;
    }
    else{
        session_destroy();
        echo '{ "success": false,
                "failure": "Invalid username and password."}';
        exit;
    }
?>
