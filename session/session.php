<?php
	session_start();
	$usersession = array('admin' => $_SESSION['admin'], 'username' => $_SESSION['username'], 'id' => $_SESSION['id'], 'fname' => $_SESSION['fname'], 'lname' => $_SESSION['lname'], 'completed' => $_SESSION['completed'], 'todo' => $_SESSION['todo'], 'sequence' => $_SESSION['sequence']);
	$response = json_encode($usersession);
	echo $response;
	exit;
?>