<?php
	require_once '../db_connect.php';
	$d = json_decode(file_get_contents("php://input"), true);
	$table = $d['table'];
	$data = $d['data'];
	$where = $d['where'];

	$result = $database->update($table, $data, $where);
	echo json_encode($result);
?>