<?php
	require_once '../db_connect.php';
	$d = json_decode(file_get_contents("php://input"), true);
	$table = $d['table'];
	$join = $d['join'];
	$columns = $d['columns'];
	$where = $d['where'];
	
	if($d['join'] == '' || !isset($d['join'])){
		$result = $database->select($table, $columns, $where);
	}
	else{
		$result = $database->select($table, $join, $columns, $where);
	}
	
	echo json_encode($result);
?>