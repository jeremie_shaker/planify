<?php
	require_once '../db_connect.php';
	$d = json_decode(file_get_contents("php://input"), true);
	$table = $d['table'];
	$columns = $d['columns'];
	$search = $d['search'];
	$replace = $d['replace'];
	$where = $d['where'];
	
	$result = $database->replace($table, $columns, $search, $replace, $where);
	echo json_encode($result);
?>