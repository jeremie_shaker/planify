<?php
	require_once '../db_connect.php';
	$d = json_decode(file_get_contents("php://input"), true);
	$table = $d['table'];
	$data = $d['data'];

	$result = $database->insert($table, $data);
	echo json_encode($result);
?>