<?php
	require_once '../db_connect.php';
	$d = json_decode(file_get_contents("php://input"), true);
	$table = $d['table'];
	$where = $d['where'];

	$result = $database->has($table, $where);
	echo json_encode($result);
?>