describe('CourseListCtrl', function() {
  beforeEach(module('planify.courselist'));
  
  var scope, CourseListCtrl, ls;

  beforeEach(inject(function($controller, $rootScope, localStorageService){
	  scope = $rootScope.$new();
	  ls = localStorageService;
      CourseListCtrl = $controller('CourseListCtrl', { $scope: scope, localStorageService : ls });
  }));

  describe('$scope.gridOptions.data', function() {
	  
	 var httpMock;
	  
	 beforeEach(inject(function($httpBackend){
		httpMock = $httpBackend;
	}));
	  
	 it("should be set to courseData when displayCourses() is called", function() {
      scope.displayCourses();
	  expect(scope.gridOptions.data).toBe(scope.courseData);
    });
	
	it("should be set to lectureData when displayLectures() is called", function() {
      scope.displayLectures();
	  expect(scope.gridOptions.data).toBe(scope.lectureData);
    });
	
	it("should be set to tutorialData when displayTutorials() is called", function() {
      scope.displayTutorials();
	  expect(scope.gridOptions.data).toBe(scope.tutorialData);
    });
	
	it("should be set to labData when displayLabs() is called", function() {
      scope.displayLabs();
	  expect(scope.gridOptions.data).toBe(scope.labData);
    });
	
	it("should ALLOW a admin to edit cells", function() {
	
		scope.gridOptions = {
			enableCellEdit : true,
		};
		
		expect(scope.gridOptions.enableCellEdit).toBe(true);
    });
	
	it("should NOT ALLOW a student to edit cells", function() {
	
		scope.gridOptions = {
			enableCellEdit : false,
		};
		
		expect(scope.gridOptions.enableCellEdit).toBe(false);
    });
	
  });
});