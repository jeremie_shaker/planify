describe('SchedulerCtrl', function() {
    beforeEach(module('planify.scheduler'), ['ngRoute', 'ngCookies', 'LocalStorageModule']);
    
    var scope, SchedulerCtrl, ls;

    beforeEach(inject(function($controller, $rootScope, localStorageService){
            scope = $rootScope.$new();
            SchedulerCtrl = $controller('SchedulerCtrl', {$scope: scope});
			ls = localStorageService;
    }));
	
	describe('SchedulerCtrl', function(){
		it("should be defined", function() {
            expect(SchedulerCtrl).toBeDefined();
        });
			
	});
	
	describe('scope.init', function(){
		
		it("should be defined", function() {
            expect(scope.init).toBeDefined();
        });
			
	});
	
	describe('masterList', function(){
		
		it("should not be null", function() {
            expect(ls.get('masterList')).not.toBe(null);
        });
			
	});

    describe('$scope.scheduler', function() {
		
		it("should define parseSections()", function() {
            var initCtrl = scope.init;
			initCtrl();
            expect(parseSections).toBeDefined();
        });
		
         it("should confirm that all courses for fall within science electives list are indeed science electives", function() {
            var elective = true;
            var initCtrl = scope.init;
			initCtrl();
            scope.sections['1']['S-ELECTIVE'].forEach(function(section){
                if(section.Elective != "science"){
                    elective = false;
                }
            });
            expect(elective).toBe(true);
        });

        it("should ensure that all schedules contain a max of 5 courses", function() {
            var fiveMax = true;
			var initCtrl = scope.init;
			initCtrl();
            scope.scheduler.schedules.forEach(function(schedule){
                if(schedule.selectedCourses.length > 5){
                    fiveMax = false;
                }
            });
            expect(fiveMax).toBe(true);
        });
        
        
    });
});