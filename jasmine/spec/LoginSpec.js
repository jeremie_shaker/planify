describe('LoginCtrl', function() {
  beforeEach(module('planify.login'), ['ngRoute', 'LocalStorageModule', 'planify.user', 'planify.student']);
  
  var scope, LoginCtrl, sessionData;

  beforeEach(inject(function($controller, $rootScope, localStorageService){
	  scope = $rootScope.$new();
	  ls = localStorageService;
      LoginCtrl = $controller('LoginCtrl', { $scope: scope, localStorageService : ls });
  }));

  describe('$scope.login', function() {
	  
	 var httpMock;
	  
	 beforeEach(inject(function($httpBackend){
		httpMock = $httpBackend;
	}));
	  
	 it("should set validate to 'false' with invalid credentials", function() {
      scope.creds = {username: "gibberish", password: "nonsense"};
	  httpMock.expectPOST("session/validate3.php").respond(false);
      scope.login();
	  httpMock.flush();
	  expect(scope.validate).toBe(false);
    });

	it("should set validate to 'true' with valid credentials", function() {
      scope.creds = {username: "Admin", password: "root"};
	  httpMock.expectPOST("session/validate3.php").respond({username : 'Admin', admin : '1'});
      scope.login();
	  httpMock.flush();
      expect(scope.validate).toBe(true);
    });
	
	it("should successfully login a student", function() {
      scope.creds = {username: "A_Cohen", password: "iplan321"};
	  httpMock.expectPOST("session/validate3.php").respond({username : 'A_Cohen', admin : '0'});
      scope.login();
	  httpMock.flush();
      expect(ls.get('username')).toBe('A_Cohen');
    });
	
	it("should not give a student admin access", function() {
      scope.creds = {username: "A_Cohen", password: "iplan321"};
	  httpMock.expectPOST("session/validate3.php").respond({username : 'A_Cohen', admin : '0'});
      scope.login();
	  httpMock.flush();
      expect(ls.get('admin')).toBe(0);
    });
	
	it("should give an admin elevated access", function() {
      scope.creds = {username: "Admin", password: "root"};
	  httpMock.expectPOST("session/validate3.php").respond({username : 'Admin', admin : '1'});
      scope.login();
	  httpMock.flush();;
      expect(ls.get('admin')).toBe('1');
    });
	
	
  });
});