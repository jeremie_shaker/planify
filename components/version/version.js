'use strict';

angular.module('planify.version', [
  'planify.version.interpolate-filter',
  'planify.version.version-directive'
])

.value('version', '0.1');
