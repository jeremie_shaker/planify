'use strict';

angular.module('planify.dashboard', ['ngRoute', 'ngCookies', 'LocalStorageModule', 'planify.user', 'planify.student'])

.config(['$routeProvider', 'localStorageServiceProvider', function($routeProvider, localStorageServiceProvider) {
    $routeProvider.when('/dashboard', {
        templateUrl: 'dashboard/dashboard.php',
        controller: 'DashboardCtrl'
    });
	localStorageServiceProvider
    .setStorageType('sessionStorage');
}])

.controller('DashboardCtrl', ['$scope', '$http', 'user', 'student', '$cookieStore', 'localStorageService', function($scope, $http, user, student, $cookieStore, localStorageService) {
	
	$scope.username = localStorageService.get('username');
	$scope.$parent.username = $scope.username;
	
	$scope.admin = localStorageService.get('admin');
	$scope.id = localStorageService.get('id');
	$scope.firstName = localStorageService.get('firstName');
	$scope.lastName = localStorageService.get('lastName');
	$scope.completed = localStorageService.get('completed');
	$scope.sequence = localStorageService.get('sequence');

    $scope.init = function(){
        $scope.$parent.hideMenu = false;
        $scope.$parent.reloadSession();
    }
}]);
