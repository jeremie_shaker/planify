<!--changed-->
<div id="left_space">



<div data-ng-init=init()>
	<div ng-show="username">
		
		<p>Username: {{username}}</p>
		<div ng-hide="admin">
			<p>Welcome {{firstName}} {{lastName}}</p>
			<p>ID: {{id}}</p>
		
		<!-- changed -->
		<div class="table-responsive t_style">
		<br>
		
			<table class="table table-striped" id="t_size">
			<th>Completed Courses</th>
			  <tr ng-repeat="course in completed">
				<td>{{ course.courseID }}</td>
			  </tr>
			</table>
		</div>
		
			<br></br>
			
		<div class="table-responsive">
			<table class="table table-striped" id="t_size">
			<th>Sequence Courses</th>
			  <tr ng-repeat="course in sequence">
				<td>{{ course.courseID }}</td>
				<td>{{ course.Name }}</td>
				<td>{{ course.Credit }}</td>
			  </tr>
			</table>
		</div>
		
		
		</div>
		<div ng-show="admin">
			<p>Administrator access enabled.</p>
		</div>
	</div>
	<div ng-show="!username">
	    Nothing to display as you're a guest.
	</div>
</div>
<!--{{sections}}-->


</div>


