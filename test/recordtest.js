(function(){
'use strict';

var config = function($routeProvider){
	$routeProvider.when('/test/recordtest', {
    templateUrl: 'test/recordtest.php',
    controller: 'TestCtrl'
  });
};

var TestCtrl = function($scope, $http, db){
	//Example of how to build a JSON object for getRecord
	//index 0 is table name, and OR is the boolean logic to apply to the conditions
	//if same field i.e. ID then simply add a space and tag to differentiate them
	var record1 = { 
		'table' : 'Student',
		'where' : { OR : {'ID' : 1242, 'ID 1' : 1133, 'ID 3' : 1134 }},
	};
	
	//Example of how to build a JSON object for updateRecord
	//index 0 is the table name, 1 is data to be updated, 2 is the WHERE clause
	var record = { 
		'table' : 'Student',
		'data' : {'FirstName' : 'Daniel'},
		'where' : {'ID' : 1242}
	};
	
	var onComplete = function(data){
		$scope.queryResult = data;
	};
	
	var onError = function(data){
		$scope.queryResult = 'Did not work';
	};
	
	db.updateRecord(record)
	.then(onComplete, onError);
};

var app = angular.module('planify.recordtest', ['ngRoute', 'planify.db']);
app.config(['$routeProvider', config]);
app.controller('TestCtrl', ['$scope', '$http', 'db', TestCtrl]);
}());
	