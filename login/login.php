<div style="height: 305px">
    <carousel interval="carouselInterval">
        <slide ng-repeat="slide in slides" active="slide.active">
            <img ng-src="{{slide.image}}" style="margin:auto;">
        </slide>
    </carousel>
</div>
<div id="login" data-ng-init="init()">
    <center><h1>Planify</h1>
    <br/>
        <form ng-submit="login()">
            <fieldset>
                <div data-alert class="alert-box">
                    <p>Sorry, this login is invalid.</p>
                    <a href="#" class="close">&times</a>
                </div>
                <ul id="credentials">
                    <li><input type="text" name="username" placeholder="Enter Username" required ng-model="creds.username"></li>
                    <li><input type="password" name="password" placeholder="Enter Password" required ng-model="creds.password"></li>
                </ul>
                <button type="submit" class="btn btn-primary"> Login </button>
                <br/>
                <br/>
                <button class="btn btn-success" ng-hide="dataLoaded"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading...</button>
                <br/>
                <a href="#/dashboard">Not a registered user? Proceed as a guest.</a>
            </fieldset>
        </form>
    </center>
</div>