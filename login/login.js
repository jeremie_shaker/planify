'use strict';

angular.module('planify.login', ['ngRoute', 'LocalStorageModule', 'planify.user', 'planify.student'])

.config(['$routeProvider', 'localStorageServiceProvider', function($routeProvider, localStorageServiceProvider) {
    $routeProvider.when('/', {
        templateUrl: 'login/login.php',
        controller: 'LoginCtrl'
    });
	
	localStorageServiceProvider
    .setStorageType('sessionStorage');
}])

.controller('LoginCtrl', ['$scope', '$http', '$location', 'student', 'user', 'localStorageService', function($scope, $http, $location, student, user, localStorageService) {
    $scope.dataLoaded = true;
    $scope.carouselInterval = 3500;
    $scope.slides = [{image:'images/slide1.png'},
                    {image:'images/slide2.png'},
                    {image:'images/slide3.png'},
                    {image:'images/slide4.png'}];


    //Credentials entered in the user input on login.php
    $scope.creds = {username: "", password: ""};
    //Calls validation.php which will update the PHP session.
    $scope.login = function(){
        $scope.dataLoaded = false;
        $http.post('session/validate3.php', $scope.creds)
            .success(function(data, status, headers, config){
                $scope.dataLoaded = true;
				student.initStudent(data);
				user.initUser(data);
				
                if(data != false){
					$scope.validate = true;
                    console.log("success");
                    //redirects to dashboard if successful
                    $location.path('/dashboard');
                }
                else{
					$scope.validate = false;
                    //pops up error box if failed
                    $('.alert-box').html(data.failure);
                    $('.alert-box').show();
                }
            })
            .error(function(data, status, headers, config){
                console.log("error connecting to PHP and DB");
                $('.alert-box').html("Unable to connect to server. Please try again later.");
                $('.alert-box').show();
            })
    }

    $scope.init = function(){
        $scope.$parent.hideMenu = true;
        $scope.$parent.reloadSession();
        if(false){
            $location.path('/dashboard');
        }
    }
}]);